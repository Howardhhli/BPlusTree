#pragma once

#include <vector>
#include <string>

class BpInteriorNode;
class BpNode
{
public:
	BpNode();
	virtual ~BpNode();

	void printKeys();
	int findInsertPos(int key);

public:
	bool isRoot; // both interior and leaf node can be the root
	bool isLeaf;

	std::vector<int> keys;

	BpInteriorNode* father;
	BpNode* leftSibling;
	BpNode* rightSibling;
};

class BpInteriorNode : public BpNode
{
public:
	BpInteriorNode();
	~BpInteriorNode();

	int findChild(int key);
	int findChild(BpNode* child);

	std::vector<BpNode*> children;
};

class BpLeafNode : public BpNode
{
public:
	BpLeafNode();
	~BpLeafNode();

	void printValues();
	int find(int key);

	std::vector<std::string> values;
};


class BpTree
{
public:
	BpTree(int capacity);
	~BpTree();

	// interfaces
	std::string find(int key);
	void insert(int key, std::string value);
	void remove(int key);
	void printKeys();
	void printValues();

	// debug
	void insert(int key);

private:
	// helpers
	BpLeafNode* findLeafNode(int key);

	void insert(BpLeafNode* leafNode, int key, std::string value);
	void insert(BpInteriorNode* interiorNode, int key, BpNode* child);
	void insertRightSibling(BpNode* leftNode, int sepKey, BpNode* rightNode);
	void split(BpLeafNode* leafNode);
	void split(BpInteriorNode* interiorNode);

	void remove(BpLeafNode* leafNode, int key);
	void remove(BpInteriorNode* inteNode, int index);
	void pumpUp(BpLeafNode* leafNode);
	void pumpUp(BpInteriorNode* inteNode);
	void redistribute(BpLeafNode* leftNode, BpLeafNode* rightNode);
	void redistribute(BpInteriorNode* leftNode, BpInteriorNode* rightNode);
	void merge(BpLeafNode* leftNode, BpLeafNode* rightNode);
	void merge(BpInteriorNode* leftNode, BpInteriorNode* rightNode);

public:
	int nodeCapacity;
	BpNode* root;
};