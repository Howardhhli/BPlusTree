#include "BpTree.h"
#include <iostream>


void testInsert(BpTree* theTree)
{
	theTree->insert(2);
	theTree->insert(11);
	theTree->insert(21);
	theTree->insert(8);
	theTree->insert(64);
	theTree->insert(5);
	theTree->insert(23);
	//theTree->insert(97);
	theTree->insert(6);
	theTree->insert(9);
	theTree->insert(19);
	theTree->insert(7);

	theTree->insert(60);
	theTree->insert(31);
	theTree->insert(45);
	theTree->insert(51);
	theTree->insert(39);
	theTree->insert(93);
	theTree->insert(77);
}

void testRemove(BpTree* theTree)
{
	//theTree->remove(19);
	//theTree->remove(45);
	theTree->remove(9);
	theTree->remove(6);
	theTree->remove(8);
	theTree->remove(23);
	theTree->remove(31);
}


void main()
{
	BpTree* theTree = new BpTree(4);
	testInsert(theTree);
	testRemove(theTree);

	std::cout << "Print keys in levels:\n";
	theTree->printKeys();
	std::cout << std::endl;

	std::cout << "Print values in order):\n";
	theTree->printValues();
	std::cout << std::endl;

	system("pause");
}