#include "BpTree.h"
#include <cstdlib>
#include <iostream>

BpNode::BpNode()
{
	isRoot = false;
	isLeaf = false;

	father = nullptr;
	leftSibling = nullptr;
	rightSibling = nullptr;
}

BpNode::~BpNode()
{
}


void BpNode::printKeys()
{
	std::cout << "[";
	for (int i = 0; i < keys.size() - 1; i++)
		std::cout << keys[i] << ", ";
	std::cout << keys.back() << "]";
}

// the new key will be inserted before the position
// suppose no duplicate keys
int BpNode::findInsertPos(int key)
{
	for (int i = 0; i < keys.size(); i++)
	{
		if (key < keys[i]) return i;
	}

	return keys.size();
}


BpInteriorNode::BpInteriorNode()
: BpNode()
{
	isLeaf = false;
}

BpInteriorNode::~BpInteriorNode()
{
	for (int i = 0; i < children.size(); i++)
	{
		delete children[i];
	}
}

int BpInteriorNode::findChild(int key)
{
	return findInsertPos(key);
}

int BpInteriorNode::findChild(BpNode* child)
{
	for (int i = 0; i < children.size(); i++)
	{
		if (children[i] == child)
			return i;
	}

	return -1;
}


BpLeafNode::BpLeafNode()
: BpNode()
{
	isLeaf = true;
}

BpLeafNode::~BpLeafNode()
{

}

void BpLeafNode::printValues()
{
	for (int i = 0; i < keys.size(); i++)
		std::cout << values[i] << std::endl;
}

int BpLeafNode::find(int key)
{
	// exact match
	for (int i = 0; i < keys.size(); i++)
	{
		if (key == keys[i]) return i;
	}

	// not found
	return -1;
}


BpTree::BpTree(int capacity)
{
	nodeCapacity = capacity;
	
	// create the root node
	root = new BpLeafNode();
	root->isRoot = true;
	root->isLeaf = true;
}

BpTree::~BpTree()
{
	// this will recursively destruct all interior nodes, if any
	if (root) delete root;
}

std::string BpTree::find(int key)
{
	BpLeafNode* leafNode = findLeafNode(key);
	int i = leafNode->find(key);
	if (i == -1)
		return std::string("FIND-NO-MATCH!!!");
	else
		return leafNode->values[i];
}

// Find a leaf node that key lays in it
BpLeafNode* BpTree::findLeafNode(int key)
{
	BpNode* currNode = root;
	while (!currNode->isLeaf) 
	{
		BpInteriorNode* currInteriorNode = (BpInteriorNode*)currNode;

		// the key lays in child_i
		int i = currInteriorNode->findChild(key);

		// move to child_i
		currNode = currInteriorNode->children[i];
	}

	return (BpLeafNode*)currNode;
}

// show the structure of the tree
// each level is printed on one line
void BpTree::printKeys()
{
	BpNode* firstNode = root;
	while (firstNode)
	{
		BpNode* curNode = firstNode;
		while (curNode)
		{
			curNode->printKeys();
			std::cout << " ";

			// move to the next sibling
			curNode = curNode->rightSibling;
		}

		// leaf node is the last level
		if (firstNode->isLeaf) break; 

		// move down to the next level
		firstNode = ((BpInteriorNode*)firstNode)->children[0];
		std::cout << std::endl;
	}
}

// print all values in the key order
void BpTree::printValues()
{
	// find the left most leaf node
	BpNode* leftMost = root;
	while (!leftMost->isLeaf)
		leftMost = ((BpInteriorNode*)leftMost)->children[0];

	// traverse the entire leaf level
	BpLeafNode* currLeaf = (BpLeafNode*)leftMost;
	while (currLeaf)
	{
		// print values
		currLeaf->printValues();

		// move to the next sibling
		currLeaf = (BpLeafNode*)currLeaf->rightSibling;
	}
}

void BpTree::insert(int key)
{
	std::string value = std::to_string(key);
	insert(key, value);
}

void BpTree::insert(int key, std::string value)
{
	BpLeafNode* leafNode = findLeafNode(key);
	insert(leafNode, key, value);
}

void BpTree::insert(BpLeafNode* currNode, int key, std::string value)
{
	// no duplication
	if (currNode->find(key) != -1)
	{
		std::cout << "Insert failed: the same key exists.\n";
		return;
	}

	// insert
	int pos = currNode->findInsertPos(key);
	currNode->keys.insert(currNode->keys.begin() + pos, key);
	currNode->values.insert(currNode->values.begin() + pos, value);

	// split if the leaf node is full
	if (currNode->keys.size() == nodeCapacity)
		split(currNode);
}

// the child goes after the key
void BpTree::insert(BpInteriorNode* currNode, int key, BpNode* child)
{
	// insert
	int pos = currNode->findInsertPos(key);
	currNode->keys.insert(currNode->keys.begin() + pos, key);
	currNode->children.insert(currNode->children.begin() + pos + 1, child);

	// split if the node is full
	if (currNode->keys.size() == nodeCapacity)
		split(currNode);
}

void BpTree::insertRightSibling(BpNode* leftNode, int sepKey, BpNode* rightNode)
{
	// sibling relations
	rightNode->leftSibling = leftNode;
	rightNode->rightSibling = leftNode->rightSibling;
	leftNode->rightSibling = rightNode;

	// insert the rightNode
	// (1) leftNode is the root
	if (leftNode->isRoot)
	{
		// create a new root
		BpInteriorNode* newRoot = new BpInteriorNode();
		newRoot->keys.push_back(sepKey);
		newRoot->children.push_back(leftNode);
		newRoot->children.push_back(rightNode);

		// update the root
		leftNode->isRoot = false;
		newRoot->isRoot = true;
		root = newRoot;

		// parent relations
		leftNode->father = newRoot;
		rightNode->father = newRoot;
	}
	// (2) the leafNode has parent node
	else {
		// parent relation
		rightNode->father = leftNode->father;

		// insert secondHalf to the parent
		insert((BpInteriorNode*)rightNode->father, sepKey, rightNode);
	}

}

void BpTree::split(BpLeafNode* currNode)
{
	// create a new node to store the second half
	BpLeafNode* secondHalf = new BpLeafNode();
	int halfCapacity = nodeCapacity >> 1;
	secondHalf->keys.insert(secondHalf->keys.begin(), 
		currNode->keys.begin() + halfCapacity, currNode->keys.end());
	secondHalf->values.insert(secondHalf->values.begin(),
		currNode->values.begin() + halfCapacity, currNode->values.end());

	// keep the first half in leafNode
	currNode->keys.resize(halfCapacity);
	currNode->values.resize(halfCapacity);

	// the new key
	int sepKey = secondHalf->keys[0];

	// insert the secondHalf into the tree
	insertRightSibling(currNode, sepKey, secondHalf);
}

void BpTree::split(BpInteriorNode* currNode)
{
	// create a new node to hold the second half
	int halfCapacity = nodeCapacity >> 1;
	BpInteriorNode* secondHalf = new BpInteriorNode();
	secondHalf->keys.insert(secondHalf->keys.begin(),
		currNode->keys.begin() + halfCapacity + 1, currNode->keys.end());
	secondHalf->children.insert(secondHalf->children.begin(),
		currNode->children.begin() + halfCapacity + 1, currNode->children.end());

	// the children in secondHalf has new parent
	for (int i = 0; i < secondHalf->children.size(); i++)
		secondHalf->children[i]->father = secondHalf;

	// the middle key
	int midKey = currNode->keys[halfCapacity];

	// keep the first half in the inteNode
	currNode->keys.resize(halfCapacity);
	currNode->children.resize(halfCapacity + 1);

	// insert seconfHalf into the tree
	insertRightSibling(currNode, midKey, secondHalf);
}


void BpTree::remove(int key)
{
	BpLeafNode* leafNode = findLeafNode(key);
	remove(leafNode, key);
}

void BpTree::remove(BpLeafNode* currNode, int key)
{
	// find the key
	int pos = currNode->find(key);
	if (pos == -1){
		std::cout << "Alert: remove failed. The key = " << key << " does not exist.\n";
		return;
	}

	// remove
	currNode->keys.erase(currNode->keys.begin() + pos);
	currNode->values.erase(currNode->values.begin() + pos);

	// if the leafNode has too few entries
	if (currNode->keys.size() * 2 < nodeCapacity)
		pumpUp(currNode);
}

void BpTree::remove(BpInteriorNode* currNode, int index)
{
	// remove the child at index and the key that is less than child_index
	currNode->keys.erase(currNode->keys.begin() + index - 1);
	currNode->children.erase(currNode->children.begin() + index);

	// if the inteNode has too few entries
	if (currNode->children.size() * 2 < nodeCapacity)
		pumpUp(currNode);
}

// pump up strategy:
// (1) redistribute with left sibling
// (2) redistribute with right sibling
// (3) merge with left sibling
// (4) merge with right sibling
void BpTree::pumpUp(BpLeafNode* currNode)
{
	// my father and two neighbor siblings
	BpInteriorNode* myFather = currNode->father;
	BpLeafNode* leftSibling = nullptr;
	BpLeafNode* rightSibling = nullptr;
	int myIndex = myFather->findChild(currNode->keys[0]);

	// (1) redistribute with the left sibling
	// currNode has the left sibling
	if (myIndex - 1 >= 0)
	{ 
		// the left sibling is more than half full
		leftSibling = (BpLeafNode*)myFather->children[myIndex - 1];
		if ((leftSibling->keys.size() - 1) * 2 >= nodeCapacity) 
		{
			redistribute(leftSibling, currNode);
			return;
		}
	}

	// (2) redistribute with the right sibling
	// currNode has the right sibling
	if (myIndex + 1 < myFather->children.size())
	{
		rightSibling = (BpLeafNode*)myFather->children[myIndex + 1];
		// the right sibling is more than half full
		if ((rightSibling->keys.size() - 1) * 2 >= nodeCapacity) 
		{
			redistribute(currNode, rightSibling);
			return;
		}
	}

	// (3) merge to the left sibling
	if (myIndex - 1 >= 0)
	{
		leftSibling = (BpLeafNode*)myFather->children[myIndex - 1];
		merge(leftSibling, currNode);
		return;
	}

	// (4) merge with right child
	if (myIndex + 1 < myFather->children.size())
	{
		rightSibling = (BpLeafNode*)myFather->children[myIndex + 1];
		merge(currNode, rightSibling);
		return;
	}
}

void BpTree::pumpUp(BpInteriorNode* currNode)
{
	// the inteNode is the root
	// in that case root has only one child, set this child to be root
	if (currNode->isRoot) {
		// the root has only one child and is not leaf
		if (currNode->children.size() == 1) 
		{
			// the new root
			root = currNode->children[0];
			root->isRoot = true;

			// garbage collection
			currNode->children.clear(); // important: avoid recursive delete
			delete currNode;
		}
		return;
	}

	// my father and two neighbor siblings
	BpInteriorNode* myFather = currNode->father;
	BpInteriorNode* leftSibling = nullptr;
	BpInteriorNode* rightSibling = nullptr;
	int myIndex = myFather->findChild(currNode);

	// (1) redistribute with the left sibling
	// currNode has the left sibling
	if (myIndex - 1 >= 0)
	{
		// the left sibling is more than half full
		leftSibling = (BpInteriorNode*)myFather->children[myIndex - 1];
		if ((leftSibling->children.size() - 1) * 2 >= nodeCapacity)
		{
			redistribute(leftSibling, currNode);
			return;
		}
	}

	// (2) redistribute with the right sibling
	// currNode has the right sibling
	if (myIndex + 1 < myFather->children.size())
	{
		rightSibling = (BpInteriorNode*)myFather->children[myIndex + 1];
		// the right sibling is more than half full
		if ((rightSibling->children.size() - 1) * 2 >= nodeCapacity)
		{
			redistribute(currNode, rightSibling);
			return;
		}
	}

	// (3) merge to the left sibling
	if (myIndex - 1 >= 0)
	{
		leftSibling = (BpInteriorNode*)myFather->children[myIndex - 1];
		merge(leftSibling, currNode);
		return;
	}

	// (4) merge with right child
	if (myIndex + 1 < myFather->children.size())
	{
		rightSibling = (BpInteriorNode*)myFather->children[myIndex + 1];
		merge(currNode, rightSibling);
		return;
	}
}

// make their no. of children average
// either left->right or right->left
void BpTree::redistribute(BpLeafNode* leftNode, BpLeafNode* rightNode)
{
	// leftNode <== rigthNode
	if (leftNode->keys.size() < rightNode->keys.size())
	{
		// move one key/value to left
		leftNode->keys.push_back(rightNode->keys.front());
		leftNode->values.push_back(rightNode->values.front());
		rightNode->keys.erase(rightNode->keys.begin());
		rightNode->values.erase(rightNode->values.begin());
	}
	// leftNode ==> rigthNode
	else 
	{
		// move one key/value to right
		rightNode->keys.insert(rightNode->keys.begin(), leftNode->keys.back());
		rightNode->values.insert(rightNode->values.begin(), leftNode->values.back());
		leftNode->keys.erase(leftNode->keys.end() - 1);
		leftNode->values.erase(leftNode->values.end() - 1);
	}

	// update the sepKey in parent
	BpInteriorNode* theFather = leftNode->father;
	int index = theFather->findChild(leftNode->keys.front());
	theFather->keys[index] = rightNode->keys.front();
}

void BpTree::redistribute(BpInteriorNode* leftNode, BpInteriorNode* rightNode)
{
	BpInteriorNode* theFather = leftNode->father;
	int leftIndex = theFather->findChild(leftNode);
	int oldSepKey = theFather->keys[leftIndex];
	int newSepKey;

	// leftNode <== rigthNode
	if (leftNode->keys.size() < rightNode->keys.size())
	{
		// the new sepKey
		newSepKey = rightNode->keys.front();

		// move
		leftNode->keys.push_back(oldSepKey);
		leftNode->children.push_back(rightNode->children.front());
		rightNode->keys.erase(rightNode->keys.begin());
		rightNode->children.erase(rightNode->children.begin());
	}
	// leftNode ==> rigthNode
	else
	{
		// the new sepKey
		newSepKey = leftNode->keys.back();

		// move
		rightNode->keys.insert(rightNode->keys.begin(), oldSepKey);
		rightNode->children.insert(rightNode->children.begin(), leftNode->children.back());
		leftNode->keys.erase(leftNode->keys.end() - 1);
		leftNode->children.erase(leftNode->children.end() - 1);
	}

	// update the sepKey in parent
	theFather->keys[leftIndex] = newSepKey;
}

void BpTree::merge(BpLeafNode* leftNode, BpLeafNode* rightNode)
{
	// move all children to the left node
	leftNode->keys.insert(leftNode->keys.end(),
		rightNode->keys.begin(), rightNode->keys.end());
	leftNode->values.insert(leftNode->values.end(),
		rightNode->values.begin(), rightNode->values.end());

	// update links
	leftNode->rightSibling = rightNode->rightSibling;

	// remove the leafNode
	delete rightNode;
	BpInteriorNode* fatherNode = leftNode->father;
	int leftIndex = fatherNode->findChild(leftNode->keys[0]);
	remove(fatherNode, leftIndex + 1);
}

void BpTree::merge(BpInteriorNode* leftNode, BpInteriorNode* rightNode)
{
	BpInteriorNode* fatherNode = leftNode->father;
	int leftIndex = fatherNode->findChild(leftNode);

	// move children to the left sibling
	leftNode->keys.push_back(fatherNode->keys[leftIndex]);
	leftNode->keys.insert(leftNode->keys.end(),
		rightNode->keys.begin(), rightNode->keys.end());
	leftNode->children.insert(leftNode->children.end(),
		rightNode->children.begin(), rightNode->children.end());

	// update links
	leftNode->rightSibling = rightNode->rightSibling;
	for (int i = 0; i < leftNode->children.size(); i++)
		leftNode->children[i]->father = leftNode;

	// remove the leafNode
	rightNode->children.clear();
	delete rightNode;
	remove(fatherNode, leftIndex + 1);
}